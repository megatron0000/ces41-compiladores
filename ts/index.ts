// var includes = require('array-includes')
// /* when Array#includes is not present */
// delete Array.prototype.includes
// includes.shim()

// import 'js-polyfills'
export * from './common'
export * from './angular-app'
export * from './lab2/lexer'
export * from './lab3/conversion'
export * from './lab3/test'